const books = [
    {
        author: "Стівен Кінг",
        name: "Сяйво",
        price: 70
    },
    {
        author: "Стівен Кінг",
        name: "Доктор Сон",
    },
    {
        author: "Стівен Кінг",
        name: "Кладовище домашніх тварин",
        price: 70
    },
    {
        author: "Стівен Кінг",
        name: "Воно",
        price: 70
    },
    {
        author: "Стівен Кінг",
        name: "Зелена миля",
        price: 40
    },
    {
        author: "Стівен Кінг",
        name: "Темна вежа",
    }
];

const newArrBooks = books.map(book => {
    const { author, name, price } = book;
    try {
        if (author && name && price) {
            return `<li><p>Author: ${author}</p><p>Book: ${name}</p><p>Price: ${price}$</p></li>`;
        } else if (!author) {
            throw new Error('Author is undefined')
        } else if (!name) {
            throw new Error('Name is undefined')
        } else if (!price) {
            throw new Error('Price is undefined')
        }
    } catch (e) {
        console.error(e);
    }
});
let newArrJoin = newArrBooks.join(" ");
const root = document.querySelector('#root')
let bookList = document.createElement('ul');
root.prepend(bookList);
bookList.insertAdjacentHTML('beforeend', newArrJoin);
