const div = document.createElement('div');
document.body.prepend(div);
const films = document.createElement('ul');
div.prepend(films)

let StarWarsFilms;
let key;
function StarWars() {

    fetch(`https://ajax.test-danit.com/api/swapi/films`)
        .then(response => {
            return response.json()
        })
        .then(data => {
            getFilms = data.results

            for (key in getFilms) {
                films.innerHTML += `<li><b>Episode Id: </b>${getFilms[key].episode_id}</li><li><b>Name films: </b>${getFilms[key].title}</li><li class="test-${key}"><b>Characters: </b></li><li><b>Opening Crawl: </b>${getFilms[key].opening_crawl}</li>`;
            }
            getFilms.forEach((item, i) => {
                item.characters.forEach((actor) => {
                    fetch(actor).then(response => {
                        return response.json()
                    }).then(data => {
                        document.getElementsByClassName(`test-${i}`)[0].append(data.name + ", ")
                    })
                })
            })
        })
}
StarWars();
