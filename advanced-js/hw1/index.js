class Employee {
    constructor(name, surname, age, salary) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.salary = salary;
    }
    get getFullName() {
        return `${this.name} + ${this.surname}`;
    }
    set getFullName(value) {
        [this.name, this.surname] = value.split(" ");
    }

}

class Programmer extends Employee {
    constructor(name, surname, age, salary, lang = []) {
        super(name, surname, age, salary);
        this.lang = lang;
    }
    get() {
        return this.salary * 3;
    }
}

const programmerRobin = new Programmer('Robin', 'Williams', 72, 90000, ['eng']);
console.log(programmerRobin);
console.log(programmerRobin.get());

const programmerDenzel = new Programmer('Denzel', 'Washington', 68, 120000, ['eng']);
console.log(programmerDenzel);
console.log(programmerDenzel.get());

const programmerHarrison = new Programmer('Harrison', 'Ford', 81, 200000, ['eng']);
console.log(programmerHarrison);
console.log(programmerHarrison.get()); 
