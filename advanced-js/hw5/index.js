const yourIP = document.querySelector('.yourIP');
const findIP = document.querySelector('.findIP');

async function getUserIpData() {
    yourIP.innerHTML = "";
    const getUserIP = await fetch(`http://api.ipify.org/?format=json`);
    const userIp = await getUserIP.json();
    const getUserAdres = await fetch(`http://ip-api.com/json/${userIp.ip}?fields=status,message,continent,country,region,city,district`);
    const userAdres = await getUserAdres.json();
    console.log(userAdres);
    yourIP.insertAdjacentHTML('afterbegin', `
    <p><b>Континент: </b>${userAdres.continent}</p>
    <p><b>Країна: </b>${userAdres.country}</p>
    <p><b>Регіон: </b>${userAdres.region}</p>
    <p><b>Місто: </b>${userAdres.city}</p>
    <p><b>Район міста: </b>${userAdres.district}</p>
  `);
}
findIP.addEventListener('click', getUserIpData)
